---
defaults:
- '""'
flags: []
minimums: []
name: viewport
types:
- viewPort
used_by: G
---
Clipping window on final drawing.

`viewport` supersedes any [`size`](#d:size) attribute. The width and height
of the viewport specify precisely the final size of the output.
