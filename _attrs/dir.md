---
defaults:
- forward(directed)
- none(undirected)
flags: []
minimums: []
name: dir
types:
- dirType
used_by: E
---
Edge type for drawing arrowheads.

Indicates which ends of the edge should be decorated with an arrowhead.

The actual style of the arrowhead can be specified using the
[`arrowhead`](#d:arrowhead) and [`arrowtail`](#d:arrowtail) attributes.

See [limitation](#h:undir_note).
